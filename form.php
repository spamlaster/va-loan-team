<!doctype html>
<html lang="en">
<head>
    
<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.min.css">


<!-- ===================
Style Sheets
=====================-->
    <link rel="stylesheet" href="../css/step-form-new.css" type="text/css" />
<!-- ===================
Script
=====================-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/step-form.js"></script>

<title>Lowest VA Rates | FORM</title>

</head>

    <body>


                      
<script>
    $(document).ready(function() {
            $('.form-container').steppy();				
    });
</script>
		
<div class="form-container">
	<div class="stepCount">
            <div class="meter orange nostripes">
                <span id="stepProgress" style=""></span>
            </div>
	</div>
        <div class="step first box-input" id="">
               <div class="progress"><p>Progress: Step 1 of 13</p></div>
                <div class="loanTypeText ">Select Loan Type</div>
               <div class="headline" style="display:none;">Select Loan Type</div>
               <input type="hidden" id="LoanType" value="LoanType" />
               <div class="appbox" data-app-type="refinance">
<!--                <img src="images/arrow.png" class="red-arrow" />-->
                <div class="appicon">
                    <img class="imgB1" src="images/red-arrow-curved.png" alt="Arrow" style="width:100px;height:110px;">
                    <img src="images/refi.png" width="116" height="91" />
                </div>
                    <div class="txt">VA Home Refinance</div>
                </div>

                <div class="appbox" data-app-type="purchase">
                    <div class="appicon">
                    <img src="images/hpurchase.png" width="107" height="91" />
                    </div>
                        <div class="txt">VA Home Purchase</div>
                </div>
        <div class="clr"></div>
        </div>


    
            <div class="step box-input" id="">
        <div class="progress"><p>Progress: Step 2 of 13</p></div>
                <div class="headline">Are you a Veteran or active Military?</div>
                <input type="hidden" id="branch" value="branch" />
               <div class="appbox">
               <div class="appicon"><img src="images/groups-military.png" width="116" height="91"></div>
               <div class="txt">Yes</div>

               </div>
               
               <div class="appbox">
               <div class="appicon"><img src="images/militry-no.png" width="116" height="91"></div>
               <div class="txt">No</div>
               </div>
                <div class="clr"></div>
        </div>
    
    
    
    <!--
       <div class="step textdrop-input" id="">
        <div class="progress"><p>Progress: Step 2 of 13</p></div>
        <div class="headline">Are you a Veteran or active Military?</div>
           <input type="hidden" id="branch" value="branch" />

               <div class="appbox" data-app-type="Yes">
               <div class="appicon"><img src="images/groups-military.png" width="116" height="91"></div>
               <div class="txt">Yes</div>

               </div>
               
               <div class="appbox" data-app-type="No">
               <div class="appicon"><img src="images/militry-no.png" width="116" height="91"></div>
               <div class="txt">No</div>
               </div>
                <div class="clr"></div>

               </div>
        </div>-->
    
        <div class="step box-input" id="">
        <div class="progress"><p>Progress: Step 3 of 13</p></div>
               <div class="headline">Select Property Type</div>
               <input type="hidden" id="PropertyType" value="PropertyType" />
               <div class="appbox">
               <div class="appicon"><img src="images/singlefamily.png" width="116" height="91"></div>
               <div class="txt">Single Family</div>

               </div>
               <div class="appbox">
               <div class="appicon"><img src="images/Multi Family.png" width="116" height="91"></div>
               <div class="txt">Multi Family</div>
               </div>
               <div class="appbox">
               <div class="appicon"><img src="images/Condo.png" width="116" height="91"></div>
               <div class="txt">Condominium</div>
               </div>

               <div class="appbox">
               <div class="appicon"><img src="images/Townhouse.png" width="116" height="91"></div>
               <div class="txt">Townhouse</div>
               </div>
               <div class="appbox">
               <div class="appicon"><img src="images/Manufactured.png" width="116" height="91"></div>
               <div class="txt">Manufactured Home</div>
               </div>
               <div class="clr"></div>

       </div>
	
        <div class="step box-input" id="">
        <div class="progress"><p>Progress: Step 4 of 13</p></div>
            <div class="headline">How Is Your Credit?</div>
                    <input type="hidden" id="CreditHistory" value="CreditHistory" />

                    <div class="appbox credit">
                    <div class="appicon"><img src="images/excellent credit.png" width="116" height="91"></div>
                    <div class="txt">Excellent</div>

                    </div>
                    <div class="appbox credit">
                    <div class="appicon"><img src="images/Very Good Credit.png" width="116" height="91"></div>
                    <div class="txt">Very Good</div>
                    </div>

                    <div class="appbox credit">
                    <div class="appicon"><img src="images/Good Credit.png" width="116" height="91"></div>
                    <div class="txt"> Good</div>
                    </div>

                    <div class="appbox credit">
                    <div class="appicon"><img src="images/Fair Credit.png" width="116" height="91"></div>
                    <div class="txt">Fair</div>
                    </div>
                    <div class="appbox credit">
                    <div class="appicon"><img src="images/poor credit.png" width="116" height="91"></div>
                    <div class="txt">Poor</div>
                    </div>

                <div class="clr"></div>
        </div>
	
        <div class="step dropdown-input" id="">
        <div class="progress"><p>Progress: Step 5 of 13</p></div>
                <div class="headline">What is your property value</div>
                <div>
                    <select name="PropertyValue">
                      <option value="Select One">Select Purchase Price</option>
                      <option value="70,000">Less than 80,000</option>
                      <option value="80,001">80,001 - 90,000</option>
                      <option value="90,001">90,001 - 100,000</option>
                      <option value="100,001">100,001 - 110,000</option>
                      <option value="110,001">110,001 - 120,000</option>
                      <option value="120,001">120,001 - 130,000</option>
                      <option value="130,001">130,001 - 140,000</option>

                      <option value="140,001">140,001 - 150,000</option>
                      <option value="150,001">150,001 - 160,000</option>
                      <option value="160,001">160,001 - 170,000</option>
                      <option value="170,001">170,001 - 180,000</option>
                      <option value="180,001">180,001 - 190,000</option>
                      <option value="190,001">190,001 - 200,000</option>

                      <option value="200,001">200,001 - 210,000</option>
                      <option value="210,001">210,001 - 220,000</option>
                      <option value="220,001">220,001 - 230,000</option>
                      <option value="230,001">230,001 - 240,000</option>
                      <option value="240,001">240,001 - 250,000</option>
                      <option value="250,001">250,001 - 275,000</option>

                      <option value="275,001">275,001 - 300,000</option>
                      <option value="300,001">300,001 - 325,000</option>
                      <option value="325,001">325,001 - 350,000</option>
                      <option value="350,001">350,001 - 375,000</option>
                      <option value="375,001">375,001 - 400,000</option>
                      <option value="400,001">400,001 - 425,000</option>

                      <option value="425,001">425,001 - 450,000</option>
                      <option value="450,001">450,001 - 475,000</option>
                      <option value="475,001">475,001 - 500,000</option>
                      <option value="525,001">525,001 - 550,000</option>
                      <option value="550,001">550,001 - 575,000</option>
                      <option value="575,001">575,001 - 600,000</option>

                      <option value="600,001">600,001 - 625,000</option>
                      <option value="625,001">625,001 - 650,000</option>
                      <option value="650,001">650,001 - 675,000</option>
                      <option value="675,001">675,001 - 700,000</option>
                      <option value="700,001">700,001 - 725,000</option>
                      <option value="725,001">725,001 - 750,000</option>

                      <option value="750,001">750,001 - 775,000</option>
                      <option value="775,001">775,001 - 800,000</option>
                      <option value="800,001">800,001 - 825,000</option>
                      <option value="825,001">825,001 - 850,000</option>
                      <option value="850,001">850,001 - 875,000</option>
                      <option value="875,001">875,001 - 900,000</option>

                      <option value="900,001">900,001 - 925,000</option>
                      <option value="925,001">925,001 - 950,000</option>
                      <option value="950,001">950,001 - 975,000</option>
                      <option value="975,001">975,001 - 1,000,000</option>
                      <option value="1,000,000 +">1,000,000 or more</option>

                    </select>
                </div>
            <div class="clr"></div>
        </div>
	
        <div class="step textdrop-input r" id="">
        <div class="progress"><p>Progress: Step 6 of 13</p></div>
                <div class="headline">What is your Mortgage balance</div>
                <div>
                    <select name="MortgageBalance">
                      <option value="Select One">Select Purchase Price</option>
                      <option value="Less than $70,000">Less than $80,000</option>
                      <option value="$80,001 - $90,000">$80,001 - $90,000</option>
                      <option value="$90,001 - $100,000">$90,001 - $100,000</option>
                      <option value="$100,001 - $110,000">$100,001 - $110,000</option>
                      <option value="$110,001 - $120,000">$110,001 - $120,000</option>
                      <option value="$120,001 - $130,000">$120,001 - $130,000</option>
                      <option value="$130,001 - $140,000">$130,001 - $140,000</option>

                      <option value="$140,001 - $150,000">$140,001 - $150,000</option>
                      <option value="$150,001 - $160,000">$150,001 - $160,000</option>
                      <option value="$160,001 - $170,000">$160,001 - $170,000</option>
                      <option value="$170,001 - $180,000">$170,001 - $180,000</option>
                      <option value="$180,001 - $190,000">$180,001 - $190,000</option>
                      <option value="$190,001 - $200,000">$190,001 - $200,000</option>

                      <option value="$200,001 - $210,000">$200,001 - $210,000</option>
                      <option value="$210,001 - $220,000">$210,001 - $220,000</option>
                      <option value="$220,001 - $230,000">$220,001 - $230,000</option>
                      <option value="$230,001 - $240,000">$230,001 - $240,000</option>
                      <option value="$240,001 - $250,000">$240,001 - $250,000</option>
                      <option value="$250,001 - $275,000">$250,001 - $275,000</option>

                      <option value="$275,001 - $300,000">$275,001 - $300,000</option>
                      <option value="$300,001 - $325,000">$300,001 - $325,000</option>
                      <option value="$325,001 - $350,000">$325,001 - $350,000</option>
                      <option value="$350,001 - $375,000">$350,001 - $375,000</option>
                      <option value="$375,001 - $400,000">$375,001 - $400,000</option>
                      <option value="$400,001 - $425,000">$400,001 - $425,000</option>

                      <option value="$425,001 - $450,000">$425,001 - $450,000</option>
                      <option value="$450,001 - $475,000">$450,001 - $475,000</option>
                      <option value="$475,001 - $500,000">$475,001 - $500,000</option>
                      <option value="$525,001 - $550,000">$525,001 - $550,000</option>
                      <option value="$550,001 - $575,000">$550,001 - $575,000</option>
                      <option value="$575,001 - $600,000">$575,001 - $600,000</option>

                      <option value="$600,001 - $625,000">$600,001 - $625,000</option>
                      <option value="$625,001 - $650,000">$625,001 - $650,000</option>
                      <option value="$650,001 - $675,000">$650,001 - $675,000</option>
                      <option value="$675,001 - $700,000">$675,001 - $700,000</option>
                      <option value="$700,001 - $725,000">$700,001 - $725,000</option>
                      <option value="$725,001 - $750,000">$725,001 - $750,000</option>

                      <option value="$750,001 - $775,000">$750,001 - $775,000</option>
                      <option value="$775,001 - $800,000">$775,001 - $800,000</option>
                      <option value="$800,001 - $825,000">$800,001 - $825,000</option>
                      <option value="$825,001 - $850,000">$825,001 - $850,000</option>
                      <option value="$850,001 - $875,000">$850,001 - $875,000</option>
                      <option value="$875,001 - $900,000">$875,001 - $900,000</option>

                      <option value="$900,001 - $925,000">$900,001 - $925,000</option>
                      <option value="$925,001 - $950,000">$925,001 - $950,000</option>
                      <option value="$950,001 - $975,000">$950,001 - $975,000</option>
                      <option value="$975,001 - $1,000,000">$975,001 - $1,000,000</option>
                      <option value="$1,000,000 +">$1,000,000 or more</option>

                    </select>
                </div>

                <div class="clr"></div>
        </div>

        <div class="step textdrop-input r" id="">
        <div class="progress"><p>Progress: Step 7 of 13</p></div>
        <div class="headline">What is your Mortgage Interest Rate</div>
        
        <div>
                <select id="MortgageRate" name="MortgageRate">
                        <option value="Select One" selected="selected">Mortgage Interest Rate</option>
                        <option value="12">+11.00%</option>
                        <option value="10.750">10.750%</option>
                        <option value="10.500">10.500%</option>
                        <option value="10.250">10.250%</option>
                        <option value="10.000">10.000%</option>
                        <option value="9.750">9.750%</option>
                        <option value="9.500">9.500%</option>
                        <option value="9.250">9.250%</option>
                        <option value="9.000">9.000%</option>
                        <option value="8.750">8.750%</option>
                        <option value="8.500">8.500%</option>
                        <option value="8.250">8.250%</option>
                        <option value="8.000">8.000%</option>
                        <option value="7.750">7.750%</option>
                        <option value="7.500">7.500%</option>
                        <option value="7.250">7.250%</option>
                        <option value="7.000">7.000%</option>
                        <option value="6.750">6.750%</option>
                        <option value="6.500">6.500%</option>
                        <option value="6.250">6.250%</option>
                        <option value="6.000">6.000%</option>
                        <option value="5.750">5.750%</option>
                        <option value="5.500">5.500%</option>
                        <option value="5.250">5.250%</option>
                        <option value="5.000">5.000%</option>
                        <option value="4.750">4.750%</option>
                        <option value="4.500">4.500%</option>
                        <option value="4.250">4.250%</option>
                        <option value="4.000">4.000%</option>
                        <option value="3.750">3.750%</option>
                        <option value="3.500">3.500%</option>
                        <option value="3.250">3.250%</option>
                        <option value="3.000">3.000%</option>
                        <option value="2.750">2.750%</option>
                        <option value="2.500">2.500%</option>
                        <option value="2.250">2.250%</option>
                </select>
                </div>
                <div class="clr"></div>
        </div>
    
        <div class="step textdrop-input p" id="">
        <div class="progress"><p>Progress: Step 8 of 13</p></div>
        <div class="headline">Estimated Loan Amount?</div>

            <div>
                <select id="EstimatedLoan" name="EstimatedLoan">
                <option value="Select One" selected="selected">Estimated Loan Amount</option>
                <option value="Less than $70,000">Less than $70,000</option>
                <option value="$70,001 - $80,000">$70,001 - $80,000</option>
                <option value="$80,001 - $90,000">$80,001 - $90,000</option>
                <option value="$90,001 - $100,000">$90,001 - $100,000</option>
                <option value="$100,001 - $110,000">$100,001 - $110,000</option>
                <option value="$110,001 - $120,000">$110,001 - $120,000</option>
                <option value="$120,001 - $130,000">$120,001 - $130,000</option>
                <option value="$130,001 - $140,000">$130,001 - $140,000</option>x

                <option value="$140,001 - $150,000">$140,001 - $150,000</option>
                <option value="$150,001 - $160,000">$150,001 - $160,000</option>
                <option value="$160,001 - $170,000">$160,001 - $170,000</option>
                <option value="$170,001 - $180,000">$170,001 - $180,000</option>
                <option value="$180,001 - $190,000">$180,001 - $190,000</option>
                <option value="$190,001 - $200,000">$190,001 - $200,000</option>

                <option value="$200,001 - $210,000">$200,001 - $210,000</option>
                <option value="$210,001 - $220,000">$210,001 - $220,000</option>
                <option value="$220,001 - $230,000">$220,001 - $230,000</option>
                <option value="$230,001 - $240,000">$230,001 - $240,000</option>
                <option value="$240,001 - $250,000">$240,001 - $250,000</option>
                <option value="$250,001 - $275,000">$250,001 - $275,000</option>

                <option value="$275,001 - $300,000">$275,001 - $300,000</option>
                <option value="$300,001 - $325,000">$300,001 - $325,000</option>
                <option value="$325,001 - $350,000">$325,001 - $350,000</option>
                <option value="$350,001 - $375,000">$350,001 - $375,000</option>
                <option value="$375,001 - $400,000">$375,001 - $400,000</option>
                <option value="$400,001 - $425,000">$400,001 - $425,000</option>

                <option value="$425,001 - $450,000">$425,001 - $450,000</option>
                <option value="$450,001 - $475,000">$450,001 - $475,000</option>
                <option value="$475,001 - $500,000">$475,001 - $500,000</option>
                <option value="$525,001 - $550,000">$525,001 - $550,000</option>
                <option value="$550,001 - $575,000">$550,001 - $575,000</option>
                <option value="$575,001 - $600,000">$575,001 - $600,000</option>

                <option value="$600,001 - $625,000">$600,001 - $625,000</option>
                <option value="$625,001 - $650,000">$625,001 - $650,000</option>
                <option value="$650,001 - $675,000">$650,001 - $675,000</option>
                <option value="$675,001 - $700,000">$675,001 - $700,000</option>
                <option value="$700,001 - $725,000">$700,001 - $725,000</option>
                <option value="$725,001 - $750,000">$725,001 - $750,000</option>

                <option value="$750,001 - $775,000">$750,001 - $775,000</option>
                <option value="$775,001 - $800,000">$775,001 - $800,000</option>
                <option value="$800,001 - $825,000">$800,001 - $825,000</option>
                <option value="$825,001 - $850,000">$825,001 - $850,000</option>
                <option value="$850,001 - $875,000">$850,001 - $875,000</option>
                <option value="$875,001 - $900,000">$875,001 - $900,000</option>

                <option value="$900,001 - $925,000">$900,001 - $925,000</option>
                <option value="$925,001 - $950,000">$925,001 - $950,000</option>
                <option value="$950,001 - $975,000">$950,001 - $975,000</option>
                <option value="$975,001 - $1,000,000">$975,001 - $1,000,000</option>
                <option value="$1,000,001 - $1,100,000">$1,000,001 - $1,100,000</option>
                <option value="$1,100,001 - $1,200,000">$1,100,001 - $1,200,000</option>

                <option value="$1,200,001 - $1,300,000">$1,200,001 - $1,300,000</option>
                <option value="$1,300,001 - $1,400,000">$1,300,001 - $1,400,000</option>
                <option value="$1,400,001 - $1,500,000">$1,400,001 - $1,500,000</option>
                <option value="$1,500,000+">$1,500,000+</option>
                </select>
            </div>

                <div class="clr"></div>
        </div>

        <div class="step box-input p" id="">
        <div class="progress"><p>Progress: Step 9 of 13</p></div>
                <div class="headline">Are you a home owner?</div>
                <input type="hidden" id="HomeOwner" value="HomeOwner" />
               <div class="appbox">
               <div class="appicon"><img src="images/Home Owner.png" width="116" height="91"></div>
               <div class="txt">Yes</div>

               </div>
               
               <div class="appbox">
               <div class="appicon"><img src="images/no home owner.png" width="116" height="91"></div>
               <div class="txt">No</div>
               </div>
                <div class="clr"></div>
        </div>


            <div class="step textdrop-input p" id="">
                <div class="progress"><p>Progress: Step 10 of 13</p></div>
                <div class="headline">Have you found a home?</div>
                
                <input type="hidden" id="FoundHome" value="FoundHome" />

               <div class="appbox">
               <div class="appicon"><img src="images/found_a_home.png" width="116" height="91"></div>
               <div class="txt">Yes</div>

               </div>
               
               <div class="appbox">
               <div class="appicon"><img src="images/home_search.png" width="116" height="91"></div>
               <div class="txt">No</div>    
               </div>
                <div class="clr"></div>
        </div>
								
	<div class="step textdrop-input p" id="">
            <div class="progress"><p>Progress: Step 11 of 13</p></div>
            <div class="headline">Are you working with a Realtor?</div>
								
            <input type="hidden" id="WorkRealtor" value="WorkRealtor" />

               <div class="appbox">
               <div class="appicon"><img src="images/real estate agent.png" width="116" height="91"></div>
               <div class="txt">Yes</div>

               </div>
               
               <div class="appbox">
               <div class="appicon"><img src="images/no realtor.png" width="116" height="91"></div>
               <div class="txt">No</div>
               </div>
                <div class="clr"></div>						
        </div>
            
        <div class="step textdrop-input" id="">
        <div class="progress"><p>Progress: Step 12 of 13</p></div>
        <div class="headline">Please enter your property information</div>
            <div>
                    <input name="Address" id="address" value="" type="text" placeholder="Address">
            </div>
            <div>
                    <input name="City" id="city" value="" type="text" placeholder="City">
            </div>
            <div>
<!--                    <input name="State" id="state" value="" type="text" placeholder="State">-->
            <select id="state" name="State">
                <option selected="selected" class="multiChoice">Select</option>
                <option class="multiChoice" value="Alabama">Alabama</option>
                <option class="multiChoice" value="Alaska">Alaska</option>
                <option class="multiChoice" value="Arizona">Arizona</option>
                <option class="multiChoice" value="Arkansas">Arkansas</option>
                <option class="multiChoice" value="California">California</option>
                <option class="multiChoice" value="Colorado">Colorado</option>
                <option class="multiChoice" value="Connecticut">Connecticut</option>
                <option class="multiChoice" value="Delaware">Delaware</option>
                <option class="multiChoice" value="Florida">Florida</option>
                <option class="multiChoice" value="Georgia">Georgia</option>
                <option class="multiChoice" value="Hawaii">Hawaii</option>
                <option class="multiChoice" value="Idaho">Idaho</option>
                <option class="multiChoice" value="Illinois">Illinois</option>
                <option class="multiChoice" value="Indiana">Indiana</option>
                <option class="multiChoice" value="Iowa">Iowa</option>
                <option class="multiChoice" value="Kansas">Kansas</option>
                <option class="multiChoice" value="Kentucky">Kentucky</option>
                <option class="multiChoice" value="Louisiana">Louisiana</option>
                <option class="multiChoice" value="Maine">Maine</option>
                <option class="multiChoice" value="Maryland">Maryland</option>
                <option class="multiChoice" value="Massachusetts">Massachusetts</option>
                <option class="multiChoice" value="Michigan">Michigan</option>
                <option class="multiChoice" value="Minnesota" >Minnesota</option>
                <option class="multiChoice" value="Mississippi" >Mississippi</option>
                <option class="multiChoice" value="Missouri" >Missouri</option>
                <option class="multiChoice" value="Montana" >Montana</option>
                <option class="multiChoice" value="Nebraska">Nebraska</option>
                <option class="multiChoice" value="Nevada">Nevada</option>
                <option class="multiChoice" value="New Hampshire">New Hampshire</option>
                <option class="multiChoice" value="New Jersey">New Jersey</option>
                <option class="multiChoice" value="New Mexico">New Mexico</option>
                <option class="multiChoice" value="New York">New York</option>
                <option class="multiChoice" value="North Carolina">North Carolina</option>
                <option class="multiChoice" value="North Dakota">North Dakota</option>
                <option class="multiChoice" value="Ohio">Ohio</option>
                <option class="multiChoice" value="Oklahoma">Oklahoma</option>
                <option class="multiChoice" value="Oregon">Oregon</option>
                <option class="multiChoice" value="Pennsylvania">Pennsylvania</option>
                <option class="multiChoice" value="Rhode Island">Rhode Island</option>
                <option class="multiChoice" value="South Carolina">South Carolina</option>
                <option class="multiChoice" value="South Dakota">South Dakota</option>
                <option class="multiChoice" value="Tennessee">Tennessee</option>
                <option class="multiChoice" value="Texas">Texas</option>
                <option class="multiChoice" value="Utah">Utah</option>
                <option class="multiChoice" value="Vermont">Vermont</option>
                <option class="multiChoice" value="Virginia">Virginia</option>
                <option class="multiChoice" value="Washington">Washington</option>
                <option class="multiChoice" value="West Virginia">West Virginia</option>
                <option class="multiChoice" value="Wisconsin">Wisconsin</option>
                <option class="multiChoice" value="Wyoming">Wyoming</option>
        </select>
            </div>
            <div>
                    <input name="Zip" id="zip" value="" type="number" placeholder="Zip">
            </div>
        

                <div class="clr"></div>
        </div>
	
        <!-- Last two steps -->
        <div class="step textdrop-input last" id="">
       <div class="progress"><p>Progress: Step 13 of 13</p></div>
              <div class="headline">Contact Information</div>

              <div>
                      <input name="FirstName" id="FirstName" value="" type="text" placeholder="First Name">
              </div>
              <div>
                      <input name="LastName" id="LastName" value="" type="text" placeholder="Last Name">
              </div>
              <div>
                      <input name="EmailAddress" id="Email" value="" type="email" placeholder="Email">
              </div>
                        
              <div>
                  <input name="HomePhone" id="phone" value="" type="text" placeholder="Phone XXXXXXXXXX" >
              </div>
              <label id="commentPhonePromt"></label>
              
              <div class="continue-but submit">Submit</div>
              
                  
        </div>
</div>	

                            <!-- step 1 -->
<!--                            <div class="step first box-input" id="">
                                <h2 class="intro text-left">
                                    Qualify for a <span class="strong colored-text">VA Loan</span> with <strong>No Down Payment</strong> and <strong>No Mortgage Insurance</strong>
				</h2>
				
				<p class="sub-heading text-left">
				    Your Military Service Saves You with VA Rates
				</p>
				
				 CTA BUTTONS 
                                <div class="loanTypeText "><label>Choose Loan Type</label></div>
                                <div class="headline" style="display:none;">Select Loan Type</div>
				<div id="cta-5" class="button-container ">
				<select class="form-control input-lg" name="state">
                                    <option>Select Loan Type</option>
                                    <option value="VA Refinance">VA Home Refinance</option>
                                    <option value="VA Purchase">VA Home Purchase</option>
                                </select>
				<a href="#section8" class="btn standard-button pull-left">Get Started</a>
				
				</div>
                            </div>-->
                            <!-- end step 1. Start step 2-->


	</body>
</html>
