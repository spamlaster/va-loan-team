<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="Responsive Bootstrap Multi-Purpose Landing Page Template">
<meta name="keywords" content="LandX, Bootstrap, Landing page, Template, Registration, Landing">
<meta name="author" content="Mizanur Rahman">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- SITE TITLE -->
<title>LandX - Responsive Multi-Purpose Landing Page</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="icon" href="images/favicon.ico">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- FONT ICONS -->
<!-- IonIcons -->
<link rel="stylesheet" href="assets/ionicons/css/ionicons.css">

<!-- Font Awesome -->
<!--<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


<!-- Elegant Icons -->
<link rel="stylesheet" href="assets/elegant-icons/style.css">
<!--[if lte IE 7]><script src="assets/elegant-icons/lte-ie7.js"></script><![endif]-->



<!-- CAROUSEL AND LIGHTBOX -->
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/nivo_themes/default/default.css">

<!-- COLORS -->
<!--<link rel="stylesheet" href="css/colors/blue.css">  DEFAULT COLOR/ CURRENTLY USING -->
<!-- <link rel="stylesheet" href="css/colors/red.css"> -->
 <link rel="stylesheet" href="css/colors/green.css"> 
<!-- <link rel="stylesheet" href="css/colors/purple.css"> -->
<!-- <link rel="stylesheet" href="css/colors/orange.css"> -->
<!-- <link rel="stylesheet" href="css/colors/blue-munsell.css"> -->
<!-- <link rel="stylesheet" href="css/colors/slate.css"> -->
<!-- <link rel="stylesheet" href="css/colors/yellow.css"> -->

<!-- CUSTOM STYLESHEETS -->
<link rel="stylesheet" href="css/styles.css">

<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="css/responsive.css">

<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<script src="js/respond.min.js"></script>
<![endif]-->

<!-- ****************
      After neccessary customization/modification, Please minify HTML/CSS according to http://browserdiet.com/en/ for better performance
     **************** -->
<style>
    header {
            background: url(http://teleportcpa.com/stockphoto/Flag%20America%20Patriotic%20Wave.jpg) no-repeat center top fixed;
            -webkit-background-size: cover;
    background-size: cover;
    }
    ul { padding-left:20px; list-style:none; }
li { margin-bottom:10px; }
li:before {    
    font-family: 'FontAwesome';
    content: '\f058';
    margin:0 5px 0 -15px;
}
 li {
        color: #FFF;
        font-size: 18px;
        padding: 5px;
    }
    .section1 {
        padding-top: 25px;
        padding-bottom: 25px;
    }    
    h2 {
        color: #FFF;
    }    

</style>
</head>

<body>
<!-- =========================
     PRE LOADER       
============================== -->
<!--<div class="preloader">
  <div class="status">&nbsp;</div>
</div>-->

<!-- =========================
     HEADER   
============================== -->
<header id="home">

<!-- COLOR OVER IMAGE -->
<div class="color-overlay">
	
	<div class="navigation-header">
		
		<!-- STICKY NAVIGATION -->
		<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
			<div class="container">
				<div class="navbar-header">
					
					<!-- LOGO ON STICKY NAV BAR -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#landx-navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					
				</div>
                            <a class="navbar-brand col-md-4" href="#"><img style="max-width: 250px;" src="http://thevaloanteam.com/pic/vaheader.png" alt=""></a>
					
                                        <div class='socialicon col-md-8 col-xs-12 text-right'>
                                        <i class="fa fa-phone-square"> 1-877-907-4762</i>
                                        </div>
                        <!-- /END NAVBAR-HEADER -->    
			</div>
			<!-- /END CONTAINER -->
			
		</div>
		
		<!-- /END STICKY NAVIGATION -->
		
		
	</div>
	
	<!-- HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
	<div class="container">
		
		<div class="row">
			
			<div class="col-md-12">
			
			<!-- SCREENSHOT -->
			<div class="" style="padding: 20px;">
				<h2 class="intro ">
                                    Thank You for requesting your <span class="strong colored-text">VA Loan Quote</span>
                                    
				</h2>
                            <p class="sub-heading" style="color: #FFF;">We will be in contact with you shortly</p>
			</div>
			
		</div>
			
			<!-- RIGHT - HEADING AND TEXTS -->
<!--			<div class="col-md-6 container " style="padding: 20px;">
			
				<p class="sub-heading text-left strong" style="font-size: 40px; color: #F5700B">
                                Whats Next:
                                <ol class="text-left">
                                    <li>Your request has been sent to our qualified lenders in our approved network.</li>
                                    <li>Our qualified lenders will contact you to help verify your eligibility</li>
                                    <li><strong class="strong colored-text">Recommended:</strong> Would you like to see if we can save you on a Home Security system? </li>
                                </ol>
				</p>
				
				 CTA BUTTONS 
				<div id="cta-5" class="button-container">
				
				<a href="#section1" class="btn secondary-button-white pull-left">Learn More</a>
				
				</div>
				
			</div>-->

		</div>
		
	</div>
	<!-- /END HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
	
</div>
<style>
    .grad {
  background: -webkit-linear-gradient(#FFF, #1974B9); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(#FFF, #1974B9); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(#FFF, #1974B9); /* For Firefox 3.6 to 15 */
  background: linear-gradient(#FFF, #1974B9); /* Standard syntax */
}
</style>
</header>
<!-- =========================
     SECTION 1   
============================== -->
<section class="section1" id="section1">

<div class="container">
	
	<!-- SECTION HEADING -->
	<div class="row ">
            <img class="thumbnail" src="images/thankyoupage.png">
 <script type="text/javascript">
function showDiv(toggle){
document.getElementById(toggle).style.display = 'block';
}
</script>
<div class="row" style="">
<!--    <div id="toggle" class="kbd" style="display:none; font-size: 35px; color: red;"><strong>Our <span style="color: #52658C">OverWatch</span> specialists will be in contact with you shortly!</strong></div>-->
    <div id="toggle" style="display: none;">
    <img src="images/red_ty_button.png">
    </div>
    <button type="button" class="btn btn-primary btn-lg col-md-12 grad" name="answer" style="font-size: 45px; border-color: #224578;border-style: solid; border-width:3px;" onclick="showDiv('toggle'); this.style.visibility='hidden'; "><strong style="color: red;">Click To Request Your FREE Home Security System!</strong></button>

</div>        
</div>
	<div class="features">
            <hr>
		<!-- FEATURES ROW 1 -->
		<div class="row">
			<!-- REQUEST HOME SECURITY -->
                        <div class="col-md-12">
			
			<!-- SINGLE FEATURE BOX -->
			<div class="">
				<div class="feature">
					<div class="icon">
<!--						<span class="fa fa-arrow-circle-down"></span>-->
					</div>
                              
				</div>
			</div>
		</div>
		
	</div>
                </div>     
</div> <!-- /END CONTAINER -->
</section>


<!-- =========================
     SECTION 10 - FOOTER 
============================== -->
<footer class="bgcolor-2">
<div class="container">
	
	
	
	<div class="copyright">
		 ©2015 VA Loan Team
	</div>
	
	<ul class="social-icons">
		<li><a href=""><span class="social_facebook_square"></span></a></li>
		<li><a href=""><span class="social_twitter_square"></span></a></li>
		<li><a href=""><span class="social_pinterest_square"></span></a></li>
		<li><a href=""><span class="social_googleplus_square"></span></a></li>
		<li><a href=""><span class="social_instagram_square"></span></a></li>
		<li><a href=""><span class="social_linkedin_square"></span></a></li>
	</ul>
	
</div>
</footer>


<!-- =========================
     SCRIPTS   
============================== -->
<script src="js/jquery.min.js"></script>

<script>
/* =================================
   LOADER                     
=================================== */
// makes sure the whole site is loaded
jQuery(window).load(function() {
	"use strict";
        // will first fade out the loading animation
	jQuery(".status").fadeOut();
        // will fade out the whole DIV that covers the website.
	jQuery(".preloader").delay(1000).fadeOut("slow");
})

</script>

<script src="js/bootstrap.min.js"></script>
<script src="js/retina-1.1.0.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.localScroll.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/nivo-lightbox.min.js"></script>
<script src="js/simple-expand.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/custom.js"></script>
<!-- ****************
      After neccessary customization/modification, Please minify JavaScript/jQuery according to http://browserdiet.com/en/ for better performance
     **************** -->
</body>
</html>