<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="Responsive Multi-Purpose Landing Page">
<meta name="keywords" content="VA Loan, Refinance, Mortgage">
<meta name="author" content="Nick Lancaster">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- SITE TITLE -->
<title>VA Loan Team</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="icon" href="images/favicon.ico">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- FONT ICONS -->
<!-- IonIcons -->
<link rel="stylesheet" href="assets/ionicons/css/ionicons.css">

<!-- Font Awesome -->
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">


<!-- Elegant Icons -->
<link rel="stylesheet" href="assets/elegant-icons/style.css">
<!--[if lte IE 7]><script src="assets/elegant-icons/lte-ie7.js"></script><![endif]-->



<!-- CAROUSEL AND LIGHTBOX -->
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/nivo_themes/default/default.css">

<!-- COLORS -->
<link rel="stylesheet" href="css/colors/blue.css"> <!-- DEFAULT COLOR/ CURRENTLY USING -->
<!-- <link rel="stylesheet" href="css/colors/red.css"> -->
<!-- <link rel="stylesheet" href="css/colors/green.css"> -->
<!-- <link rel="stylesheet" href="css/colors/purple.css"> -->
<!-- <link rel="stylesheet" href="css/colors/orange.css"> -->
<!-- <link rel="stylesheet" href="css/colors/blue-munsell.css"> -->
<!-- <link rel="stylesheet" href="css/colors/slate.css"> -->
<!-- <link rel="stylesheet" href="css/colors/yellow.css"> -->

<!-- CUSTOM STYLESHEETS -->
<link rel="stylesheet" href="css/styles.css">

<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="css/responsive.css">

<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<script src="js/respond.min.js"></script>
<![endif]-->

<!-- ****************
      After neccessary customization/modification, Please minify HTML/CSS according to http://browserdiet.com/en/ for better performance
     **************** -->
<!-- ===================
Style Sheets
=====================-->
    <link rel="stylesheet" href="../css/step-form-new.css" type="text/css" />
<!-- ===================
Script
=====================-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="js/step-form.js"></script>


</head>

<body>
<!-- =========================
     PRE LOADER       
============================== -->
<!--<div class="preloader">
  <div class="status">&nbsp;</div>
</div>-->

<!-- =========================
     HEADER   
============================== -->
<header id="home">

<!-- COLOR OVER IMAGE -->
<div class="color-overlay">
	
	<div class="navigation-header">
		
		<!-- STICKY NAVIGATION -->
		<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
			<div class="container">
				<div class="navbar-header">
					
					<!-- LOGO ON STICKY NAV BAR -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#landx-navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					
					
				</div>
                            <!-- ../images/vaheader_light.png -->
				<a class="navbar-brand col-md-4" href="#"><img style="max-width: 250px;" src="http://thevaloanteam.com/pic/vaheader.png" alt=""></a>
				<!-- NAVIGATION LINKS -->
<!--				<div class="navbar-collapse collapse" id="landx-navigation">
					<ul class="nav navbar-nav navbar-right main-navigation">
						<li><a href="#home">Home</a></li>
						<li><a href="#section1">Features</a></li>
						<li><a href="#section3">Brief</a></li>
						<li><a href="#section4">Pricing</a></li>
						<li><a href="#section5">Video</a></li>
						<li><a href="#section6">Screenshots</a></li>
						<li><a href="#section7">Testimonial</a></li>
						<li><a href="#section8">Contact</a></li>
					</ul>
				</div>-->
                                <div class='socialicon col-md-8 col-xs-12 text-right'>
                                    <i class="fa fa-phone-square"> 1-877-907-4762</i>
                                </div>
				
			</div>
			<!-- /END CONTAINER -->
			
		</div>
		
		<!-- /END STICKY NAVIGATION -->
		

		<!-- /END ONLY LOGO ON HEADER -->
		
	</div>
	
	<!-- HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
	<div class="container">
		
		<div class="row">
			
			<div class="col-md-6">
			
			<!-- SCREENSHOT -->
			<div class="home-screenshot side-screenshot pull-left">
<!--				<img src="../images/military.png" alt="Feature" class="img-responsive">-->
			</div>
			
		</div>

                    
                    
			<!-- RIGHT - HEADING AND TEXTS -->
			<div class="col-md-6">
<script>
    $(document).ready(function() {
            $('.form-container').steppy();				
    });
</script>
<style>
    .headline {
        color: #008ED6;
    }
 h3 {
        color: #008ED6;
    }
</style>
                            
<div class="form-container" style="margin-top: 30px; margin-bottom:0px; min-height: 200px;">
	<div class="stepCount">
            <div class="meter orange nostripes">
                <span id="stepProgress" style=""></span>
            </div>
	</div>

    <h2 class="intro text-left">
        Qualify for a <span class="strong colored-text">VA Loan</span> with <strong>No Down Payment</strong> and <strong>No Mortgage Insurance</strong>
    </h2>



</div>
    
<div class="form-container" style="text-align: center;">
	<div class="stepCount">
            <div class="meter orange nostripes">
                <span id="stepProgress" style=""></span>
            </div>
	</div>

<!--        <div class="step first box-input" id="">
    <p class="sub-heading text-left"><h3>Your Military Service Saves You on VA Rates</h3></p>
                <div class="loanTypeText ">Select Loan Type</div>
               <div class="headline" style="display:none;">Select Loan Type</div>
               <input type="hidden" id="LoanType" value="LoanType" />
               <div class="appbox" data-app-type="refinance">
                <img src="images/arrow.png" class="red-arrow" />
                <div class="appicon">
                    <img class="imgB1" src="images/red-arrow-curved.png" alt="Arrow" style="width:100px;height:110px;">
                    <img src="images/refi.png" width="116" height="91" />
                </div>
                    <div class="txt">VA Home Refinance</div>
                </div>

                <div class="appbox" data-app-type="purchase">
                    <div class="appicon">
                    <img src="images/hpurchase.png" width="107" height="91" />
                    </div>
                        <div class="txt">VA Home Purchase</div>
                </div>-->
               
        <div class="step first textdrop-input " id="">
            <p class="sub-heading text-left"><h3>Select Loan Type</h3></p>
        <div>
            
            <select id="LoanType" class="text-center" name="LoanType">
                    <option class="appbox" data-app-type="refinance" value="Refinance">VA Home Refinance</option>
                    <option class="appbox" data-app-type="purchase" value="VA Home Purchase">VA Home Purchase</option>
            </select>

            
            </div>
  
               
            <div>
<!--                    <input name="State" id="state" value="" type="text" placeholder="State">-->
            <p class="sub-heading text-left"><h3>Select State</h3></p>
            <select id="state" name="State" required>
                <option selected="selected" class="multiChoice">Select One</option>
                <option class="multiChoice" value="Alabama">Alabama</option>
                <option class="multiChoice" value="Alaska">Alaska</option>
                <option class="multiChoice" value="Arizona">Arizona</option>
                <option class="multiChoice" value="Arkansas">Arkansas</option>
                <option class="multiChoice" value="California">California</option>
                <option class="multiChoice" value="Colorado">Colorado</option>
                <option class="multiChoice" value="Connecticut">Connecticut</option>
                <option class="multiChoice" value="Delaware">Delaware</option>
                <option class="multiChoice" value="Florida">Florida</option>
                <option class="multiChoice" value="Georgia">Georgia</option>
                <option class="multiChoice" value="Hawaii">Hawaii</option>
                <option class="multiChoice" value="Idaho">Idaho</option>
                <option class="multiChoice" value="Illinois">Illinois</option>
                <option class="multiChoice" value="Indiana">Indiana</option>
                <option class="multiChoice" value="Iowa">Iowa</option>
                <option class="multiChoice" value="Kansas">Kansas</option>
                <option class="multiChoice" value="Kentucky">Kentucky</option>
                <option class="multiChoice" value="Louisiana">Louisiana</option>
                <option class="multiChoice" value="Maine">Maine</option>
                <option class="multiChoice" value="Maryland">Maryland</option>
                <option class="multiChoice" value="Massachusetts">Massachusetts</option>
                <option class="multiChoice" value="Michigan">Michigan</option>
                <option class="multiChoice" value="Minnesota" >Minnesota</option>
                <option class="multiChoice" value="Mississippi" >Mississippi</option>
                <option class="multiChoice" value="Missouri" >Missouri</option>
                <option class="multiChoice" value="Montana" >Montana</option>
                <option class="multiChoice" value="Nebraska">Nebraska</option>
                <option class="multiChoice" value="Nevada">Nevada</option>
                <option class="multiChoice" value="New Hampshire">New Hampshire</option>
                <option class="multiChoice" value="New Jersey">New Jersey</option>
                <option class="multiChoice" value="New Mexico">New Mexico</option>
                <option class="multiChoice" value="New York">New York</option>
                <option class="multiChoice" value="North Carolina">North Carolina</option>
                <option class="multiChoice" value="North Dakota">North Dakota</option>
                <option class="multiChoice" value="Ohio">Ohio</option>
                <option class="multiChoice" value="Oklahoma">Oklahoma</option>
                <option class="multiChoice" value="Oregon">Oregon</option>
                <option class="multiChoice" value="Pennsylvania">Pennsylvania</option>
                <option class="multiChoice" value="Rhode Island">Rhode Island</option>
                <option class="multiChoice" value="South Carolina">South Carolina</option>
                <option class="multiChoice" value="South Dakota">South Dakota</option>
                <option class="multiChoice" value="Tennessee">Tennessee</option>
                <option class="multiChoice" value="Texas">Texas</option>
                <option class="multiChoice" value="Utah">Utah</option>
                <option class="multiChoice" value="Vermont">Vermont</option>
                <option class="multiChoice" value="Virginia">Virginia</option>
                <option class="multiChoice" value="Washington">Washington</option>
                <option class="multiChoice" value="West Virginia">West Virginia</option>
                <option class="multiChoice" value="Wisconsin">Wisconsin</option>
                <option class="multiChoice" value="Wyoming">Wyoming</option>
        </select>
            </div>
               
        <button class="fwd btn btn-lg btn-primary">Get Started</button>  
        
        <div class="clr"></div>
        
        </div>

    
            <div class="step textdrop-input r" id="">
                <h3>What is your Mortgage balance</h3>
                <div>
                    <select name="MortgageBalance">
                      <option value="Select One">Select Purchase Price</option>
                      <option value="Less than $70,000">Less than $80,000</option>
                      <option value="$80,001 - $90,000">$80,001 - $90,000</option>
                      <option value="$90,001 - $100,000">$90,001 - $100,000</option>
                      <option value="$100,001 - $110,000">$100,001 - $110,000</option>
                      <option value="$110,001 - $120,000">$110,001 - $120,000</option>
                      <option value="$120,001 - $130,000">$120,001 - $130,000</option>
                      <option value="$130,001 - $140,000">$130,001 - $140,000</option>

                      <option value="$140,001 - $150,000">$140,001 - $150,000</option>
                      <option value="$150,001 - $160,000">$150,001 - $160,000</option>
                      <option value="$160,001 - $170,000">$160,001 - $170,000</option>
                      <option value="$170,001 - $180,000">$170,001 - $180,000</option>
                      <option value="$180,001 - $190,000">$180,001 - $190,000</option>
                      <option value="$190,001 - $200,000">$190,001 - $200,000</option>

                      <option value="$200,001 - $210,000" selected="selected">$200,001 - $210,000</option>
                      <option value="$210,001 - $220,000">$210,001 - $220,000</option>
                      <option value="$220,001 - $230,000">$220,001 - $230,000</option>
                      <option value="$230,001 - $240,000">$230,001 - $240,000</option>
                      <option value="$240,001 - $250,000">$240,001 - $250,000</option>
                      <option value="$250,001 - $275,000">$250,001 - $275,000</option>

                      <option value="$275,001 - $300,000">$275,001 - $300,000</option>
                      <option value="$300,001 - $325,000">$300,001 - $325,000</option>
                      <option value="$325,001 - $350,000">$325,001 - $350,000</option>
                      <option value="$350,001 - $375,000">$350,001 - $375,000</option>
                      <option value="$375,001 - $400,000">$375,001 - $400,000</option>
                      <option value="$400,001 - $425,000">$400,001 - $425,000</option>

                      <option value="$425,001 - $450,000">$425,001 - $450,000</option>
                      <option value="$450,001 - $475,000">$450,001 - $475,000</option>
                      <option value="$475,001 - $500,000">$475,001 - $500,000</option>
                      <option value="$525,001 - $550,000">$525,001 - $550,000</option>
                      <option value="$550,001 - $575,000">$550,001 - $575,000</option>
                      <option value="$575,001 - $600,000">$575,001 - $600,000</option>

                      <option value="$600,001 - $625,000">$600,001 - $625,000</option>
                      <option value="$625,001 - $650,000">$625,001 - $650,000</option>
                      <option value="$650,001 - $675,000">$650,001 - $675,000</option>
                      <option value="$675,001 - $700,000">$675,001 - $700,000</option>
                      <option value="$700,001 - $725,000">$700,001 - $725,000</option>
                      <option value="$725,001 - $750,000">$725,001 - $750,000</option>

                      <option value="$750,001 - $775,000">$750,001 - $775,000</option>
                      <option value="$775,001 - $800,000">$775,001 - $800,000</option>
                      <option value="$800,001 - $825,000">$800,001 - $825,000</option>
                      <option value="$825,001 - $850,000">$825,001 - $850,000</option>
                      <option value="$850,001 - $875,000">$850,001 - $875,000</option>
                      <option value="$875,001 - $900,000">$875,001 - $900,000</option>

                      <option value="$900,001 - $925,000">$900,001 - $925,000</option>
                      <option value="$925,001 - $950,000">$925,001 - $950,000</option>
                      <option value="$950,001 - $975,000">$950,001 - $975,000</option>
                      <option value="$975,001 - $1,000,000">$975,001 - $1,000,000</option>
                      <option value="$1,000,000 +">$1,000,000 or more</option>

                    </select>
                </div>

                <div class="clr"></div>
        </div>

            <div class="step dropdown-input " id="">
                <h3>What is your property value</h3>
                <div>
                    <select name="PropertyValue">
                      <option value="Select One">Select Purchase Price</option>
                      <option value="70,000">Less than 80,000</option>
                      <option value="80,001">80,001 - 90,000</option>
                      <option value="90,001">90,001 - 100,000</option>
                      <option value="100,001">100,001 - 110,000</option>
                      <option value="110,001">110,001 - 120,000</option>
                      <option value="120,001">120,001 - 130,000</option>
                      <option value="130,001">130,001 - 140,000</option>

                      <option value="140,001">140,001 - 150,000</option>
                      <option value="150,001">150,001 - 160,000</option>
                      <option value="160,001">160,001 - 170,000</option>
                      <option value="170,001">170,001 - 180,000</option>
                      <option value="180,001">180,001 - 190,000</option>
                      <option value="190,001">190,001 - 200,000</option>

                      <option value="200,001" selected="selected">200,001 - 210,000</option>
                      <option value="210,001">210,001 - 220,000</option>
                      <option value="220,001">220,001 - 230,000</option>
                      <option value="230,001">230,001 - 240,000</option>
                      <option value="240,001">240,001 - 250,000</option>
                      <option value="250,001">250,001 - 275,000</option>

                      <option value="275,001">275,001 - 300,000</option>
                      <option value="300,001">300,001 - 325,000</option>
                      <option value="325,001">325,001 - 350,000</option>
                      <option value="350,001">350,001 - 375,000</option>
                      <option value="375,001">375,001 - 400,000</option>
                      <option value="400,001">400,001 - 425,000</option>

                      <option value="425,001">425,001 - 450,000</option>
                      <option value="450,001">450,001 - 475,000</option>
                      <option value="475,001">475,001 - 500,000</option>
                      <option value="525,001">525,001 - 550,000</option>
                      <option value="550,001">550,001 - 575,000</option>
                      <option value="575,001">575,001 - 600,000</option>

                      <option value="600,001">600,001 - 625,000</option>
                      <option value="625,001">625,001 - 650,000</option>
                      <option value="650,001">650,001 - 675,000</option>
                      <option value="675,001">675,001 - 700,000</option>
                      <option value="700,001">700,001 - 725,000</option>
                      <option value="725,001">725,001 - 750,000</option>

                      <option value="750,001">750,001 - 775,000</option>
                      <option value="775,001">775,001 - 800,000</option>
                      <option value="800,001">800,001 - 825,000</option>
                      <option value="825,001">825,001 - 850,000</option>
                      <option value="850,001">850,001 - 875,000</option>
                      <option value="875,001">875,001 - 900,000</option>

                      <option value="900,001">900,001 - 925,000</option>
                      <option value="925,001">925,001 - 950,000</option>
                      <option value="950,001">950,001 - 975,000</option>
                      <option value="975,001">975,001 - 1,000,000</option>
                      <option value="1,000,000 +">1,000,000 or more</option>

                    </select>
                </div>
            <div class="clr"></div>
        </div>
    
            <div class="step textdrop-input r" id="">
        <h3>Current Interest Rate</h3>
        <div>
                <select id="MortgageRate" name="MortgageRate">
                        <option value="Select One" selected="selected">Mortgage Interest Rate</option>
                        <option value="3">Under 3.5%</option>
                        <option value="3.500">3.50% - 3.74%</option>
                        <option value="3.75">3.75% - 3.99%</option>
                        <option value="4.0">4.00% - 4.24%</option>
                        <option value="4.25">4.25% - 4.49%</option>
                        <option value="4.5">4.50% - 4.74%</option>                        
                        <option value="4.75">4.75% - 4.99%</option>
                        <option value="5.0" selected="selected">5.00% - 5.24%</option>
                        <option value="5.25">5.25% - 5.49%</option>
                        <option value="5.5">5.50% - 5.74%</option>                        
                        <option value="5.75">5.75% - 5.99%</option>                        
                        <option value="6.0">6.00% - 6.24%</option>
                        <option value="6.25">6.25% - 6.49%</option>
                        <option value="6.5">6.50% - 6.74%</option>                        
                        <option value="6.75">6.75% - 6.99%</option>
                        <option value="7">7.00% - 7.50</option>
                        <option value="8">Over 7.5%</option>
                </select>
                </div>
                <div class="clr"></div>
        </div>
    
        <div class="step textdrop-input " id="">
        <h3>Estimate Your Credit</h3>
        <div>
                <select id="creditHistory" name="creditHistory">
                        <option value="Excellent">Excellent</option>
                        <option value="Very Good">Very Good</option>
                        <option value="Good" selected="selected">Good</option>
                        <option value="Fair">Fair</option>
                        <option value="Poor">Poor</option>
                </select>
                </div>
                <div class="clr"></div>
        </div>

        <div class="step textdrop-input" id="">
<!--        <p class="sub-heading text-left"><h3>Your Military Service Saves You on VA Rates</h3></p>-->
            <h3>Property Street Address</h3>        
            <div>
                    <input name="Address" id="address" value="" type="text" placeholder="Address">
            </div>
            <h3>Property Zip Code</h3>        
            <div>
                    <input name="zip" id="zip" value="" type="text" placeholder="Zip Code">
            </div>            
                <div class="clr"></div>
        </div>
    
       <div class="step textdrop-input" id="">
           <h3>Contact Information</h3>  
              <div>
                      <input name="FirstName" id="FirstName" value="" type="text" placeholder="First Name">
              </div>
              <div>
                      <input name="LastName" id="LastName" value="" type="text" placeholder="Last Name">
              </div>            

       </div>
<div class="step textdrop-input">
              <div class="">
                  <h3>Have you or your spouse served in the military?</h3> 
                  <div class="loanTypeText sub-heading">Veterans may be eligible for special loan programs.</div>
                  <select id="branch" name="branch">
                        <option value="Yes" selected="selected">Yes</option>
                        <option value="No">No</option>
                </select>
                  
              </div>
</div>
        
        <div class="step textdrop-input last" id="">
                        
              <div>
                  <input name="HomePhone" id="phone" value="" type="text" placeholder="Phone XXXXXXXXXX" >
              </div>
              <div>
                      <input name="EmailAddress" id="Email" value="" type="email" placeholder="Email">
              </div>
            
    
              
              
            
<!--                <div  style="font-size: 12px;">We take your <a href="privacy.html" data-toggle="modal" data-toggle="modal" data-target="#myModal">Privacy</a> seriously. By clicking the button, you agree to be matched with <a href="partners.html" onclick="return popitup('partners.php')"> partners</a> from our network including New American Funding, loanDepot, and consent (not required as a condition to purchase a good/service) for us and/or them to contact you (including through automated means; e.g. autodialing, text and pre recorded messaging) via telephone, mobile device (including SMS and MMS) and/or email, even if you are on a corporate, state or national Do Not Call Registry.</div>-->
               
        </div>
        
</div>
<img style="max-width: 100px;" src="images/cert.png" >    
<!--<img src="images/privacy-seal.png" >-->
			</div>

		</div>
		
	</div>
	<!-- /END HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
	
</div>

</header>

<!-- =========================
     SECTION 1   
============================== -->

<section class="section2 bgcolor-2" id="section2">
<div class="container">
	
	<div class="row">
		
		<div class="col-md-6">
			
			<!-- DETAILS WITH LIST -->
			<div class="brief text-left">
				
				<!-- HEADING -->
				<h2>VA Home Loans Benefits </h2>
				<div class="colored-line pull-left">
				</div>
				
				<!-- TEXT -->
				<p>
					VA home loans remove many of the traditional barriers to homeownership. As an eligible Veteran or active duty service member, you have access to lower upfront and monthly costs and an easier approval process.
				</p>
				
				<!-- FEATURE LIST -->
				<ul class="feature-list-2">
					
					<!-- FEATURE -->
					<li>
					<!-- ICON -->
					<div class="icon-container pull-left">
						<span class="fa fa-money"></span>
					</div>
					<!-- DETAILS -->
					<div class="details pull-left">
						<h6>Buy a home with $0 down</h6>
						<p>
							
						</p>
					</div>
					</li>
					
					<!-- FEATURE -->
					<li>
					<!-- ICON -->
					<div class="icon-container pull-left">
						<span class="fa fa-home"></span>
					</div>
					<!-- DETAILS -->
					<div class="details pull-left">
						<h6>Refinance to lower payments</h6>
						<p>
							
						</p>
					</div>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="col-md-6">
			<!-- SCREENSHOT -->
			<div class="side-screenshot2 pull-right">
                            <div class="well">
				<img src="images/military-family.jpeg" alt="Feature" class="img-responsive">
                            </div>
                            <p><h3>Get the most ouf of your VA Loan Benefits</h3></p>
                            
			</div>
		</div>
		
	</div> <!-- END ROW -->
	
</div> <!-- END CONTAINER -->

</section>


<!--
<!-- =========================
     SECTION 2   
============================== -->
<section class="section1" id="section1">

<div class="container">
	
	<!-- SECTION HEADING -->
	
	<h2>VA Home Loan Benefits</h2>
	
	<div class="colored-line">
	</div>
	
	<div class="sub-heading">
		VA Loans for Veterans and Active Duty
	</div>
	
	<div class="features">
		
		<!-- FEATURES ROW 1 -->
		<div class="row">
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="fa fa-check-square-o"></span>
					</div>
					<h4>100%Financing - Nothing Down</h4>
					<p>
                                            With a VA home loan you are not require you to put a down payment.  
					</p>
				</div>
			</div>
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="fa fa-check-square-o"></span>
					</div>
					<h4>Credit Requirements</h4>
					<p>
						You could be approved for a VA home loan even if you have less than perfect credit. 
					</p>
				</div>
			</div>
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="fa fa-check-square-o"></span>
					</div>
					<h4>Low rates</h4>
					<p>
						Take Advantage of VA home loan low rates.
					</p>
				</div>
			</div>
		</div>
		
		<!-- FEATURES ROW 2 -->
		<div class="row">
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="fa fa-check-square-o"></span>
					</div>
					<h4>Easy Refinancing</h4>
					<p>
						VA Loans do not require verification of income, property value or assets.
					</p>
				</div>
			</div>
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="fa fa-check-square-o"></span>
					</div>
					<h4>No Mortgage Insurance</h4>
					<p>
						No mortgage insurance premium for Veterans.
					</p>
				</div>
			</div>
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="fa fa-check-square-o"></span>
					</div>
					<h4>VA Approved Lender Network</h4>
					<p>
						VA mortgage's provide huge benefits compared to other available financing options.
					</p>
				</div>
			</div>
		</div> 
	</div>
	
</div> <!-- /END CONTAINER -->
</section>



<!-- =========================
     SECTION 10 - FOOTER 
============================== -->
<footer class="bgcolor-2">
    <div>
When inquiring about a mortgage on this site, this is not a mortgage application. Upon the completion of your inquiry, we will work hard to match you with a lender who may assist you with a mortgage application and provide mortgage product eligibility requirements for your individual situation.
Any mortgage product that a lender may offer you will carry fees or costs including closing costs, origination points, and/or refinancing fees. In many instances, fees or costs can amount to several thousand dollars and can be due upon the origination of the mortgage credit product.
When applying for a mortgage credit product, lenders will commonly require you to provide a valid social security number and submit to a credit check . Consumers who do not have the minimum acceptable credit required by the lender are unlikely to be approved for mortgage refinancing.
Minimum credit ratings may vary according to lender and mortgage product. In the event that you do not qualify for a credit rating based on the required minimum credit rating, a lender may or may not introduce you to a credit counseling service or credit improvement company who may or may not be able to assist you with improving your credit for a fee.
© 2015 VALoanTeam.com is not a government agency. Not affiliated with HUD, FHA, VA, FNMA or GNMA. We work hard to match you with local lenders for the mortgage you inquire about. This is not an offer to lend and we are not affiliated with your current mortgage servicer.</div>
<p>VA Loan Team
826 E. State Rd. 2nd Floor
American Fork, Utah 84003</p>
<div class="container">
	
	<div class="footer-logo text-left">
<!--            <img width="250px" src="../images/vaheader.png" alt="">-->
	</div>
    
    
    
	
	<div class="copyright">
		 ©2015 VA Loan Team.
	</div>
	
	<ul class="social-icons">
		<li><a href=""><span class="social_facebook_square"></span></a></li>
		<li><a href=""><span class="social_twitter_square"></span></a></li>
		<li><a href=""><span class="social_pinterest_square"></span></a></li>
		<li><a href=""><span class="social_googleplus_square"></span></a></li>
		<li><a href=""><span class="social_instagram_square"></span></a></li>
		<li><a href=""><span class="social_linkedin_square"></span></a></li>
	</ul>
	
</div>
</footer>


<!-- =========================
     SCRIPTS   
============================== -->
<!--<script src="js/jquery.min.js"></script>-->

<script>
/* =================================
   LOADER                     
=================================== */
// makes sure the whole site is loaded
jQuery(window).load(function() {
	"use strict";
        // will first fade out the loading animation
	jQuery(".status").fadeOut();
        // will fade out the whole DIV that covers the website.
	jQuery(".preloader").delay(1000).fadeOut("slow");
})

</script>
<!--
<script src="js/bootstrap.min.js"></script>
<script src="js/retina-1.1.0.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.localScroll.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/nivo-lightbox.min.js"></script>
<script src="js/simple-expand.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/custom.js"></script>-->
<!-- ****************
      After neccessary customization/modification, Please minify JavaScript/jQuery according to http://browserdiet.com/en/ for better performance
     **************** -->
</body>
</html>