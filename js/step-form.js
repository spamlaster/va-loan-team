$.fn.steppy = function() {
	var currentStep = 1;
	var postItems = [];
	var formType = '';
	var hasCoborrower = '';
	var stepCount = 0;
	var humanStep = 1;
	var percent = 0;
	 var per = 0;
	
	//Get form type: Purchase or Refi
	$('.first > .appbox').click(function() {
		var val = $(this).find('.txt').html();
		if(val == 'VA Home Purchase'){
			formType = 'p';
			postItems = [];
			$('#step2 .appbox').removeClass('active');
			stepCount = $('.step:not(.r, .e)').length;
			//console.log(stepCount);
			return formType;
		}else if(val == 'VA Home Refinance') {
			formType = 'r';
			postItems = [];
			$('#step2 .appbox').removeClass('active');
			stepCount = $('.step:not(.p, .e)').length;
			//console.log(stepCount);
			return formType;
		}else {
			formType = 'e';
			postItems = [];
			$('#step2 .appbox').removeClass('active');
			stepCount = $('.step:not(.r, .p)').length;
			//console.log(stepCount);
			return formType;
		}
	});
        
        //VALIDATION **********************************************************************************************
        function validateAddress(){
            var address = $('#address').val();
            var re = /^[a-zA-Z\s\d\/]*\d[a-zA-Z\s\d\/]*$/;
            return re.test(address);
        }
        
        function validateState(){
            var state = $('#state').val();
            var re = /[a-zA-Z]+/;
            return re.test(state);
        }
        
        function validateCity(){
            var city = $('#city').val();
            var re = /[a-zA-Z]+/;
            return re.test(city);
        }
        
	function validateEmail() {
        var email = $('#Email').val();
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
        }
        
        
	//Go to Next Step
	function nextStep(){
            //Form validation *********************
            
            
            
            
		var i = 0;
		$('#step' + currentStep).hide();
		currentStep++;
		
		
		if(formType == 'p'){
			while(i < 5){
				if($('#step'+currentStep).hasClass('r') || $('#step'+currentStep).hasClass('e')){
					currentStep++;
				}else if($('#step'+currentStep).hasClass('p')) {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep++;
					percent = humanStep/stepCount;
					per = percent.toFixed(2)*100;
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+per);
					//$('#stepProgress').css('width', per+'%');
					return currentStep;
				}else {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep++;
					percent = humanStep/stepCount;
					per = percent.toFixed(2)*100;
			//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+per);
			//$('#stepProgress').css('width', per+'%');
					return currentStep;
				}
			}
		}else if(formType == 'r'){
			while(i < 5){
				if($('#step'+currentStep).hasClass('p') || $('#step'+currentStep).hasClass('e')){
					currentStep++;
				}else if($('#step'+currentStep).hasClass('r')) {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep++;
					percent = humanStep/stepCount;
					percent = Math.floor(percent);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}else {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep++;
					percent = humanStep/stepCount;
					percent = Math.floor(percent);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}
			}
			
		}else if(formType == 'e'){
			while(i < 5){
				if($('#step'+currentStep).hasClass('r') || $('#step'+currentStep).hasClass('p')){
					currentStep++;
				}else if($('#step'+currentStep).hasClass('e')) {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep++;
					percent = Math.floor(humanStep/stepCount);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}else {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep++;
					percent = Math.floor(humanStep/stepCount);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}
			}
			
		}else {
		
			$('#step' + currentStep).fadeIn('fast');
			humanStep++;
			percent = humanStep/stepCount;
			per = percent.toFixed(2)*100;
			//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+per);
			//$('#stepProgress').css('width', per+'%');
			return currentStep;
		}
		
	}
	
	//Go To Previous Step
	function prevStep(){
		var i = 0;
		$('#step' + currentStep).hide();
		currentStep = currentStep - 1;
		
		
		if(formType == 'p'){
			while(i < 5){
				if($('#step'+currentStep).hasClass('r') || $('#step'+currentStep).hasClass('e')){
					currentStep = currentStep - 1;
				}else if($('#step'+currentStep).hasClass('p')) {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep--;
					percent = Math.floor(humanStep/stepCount);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}else {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep--;
					percent = Math.floor(humanStep/stepCount);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}
			}
		}else if(formType == 'r'){
			while(i < 5){
				if($('#step'+currentStep).hasClass('p') || $('#step'+currentStep).hasClass('e')){
					currentStep = currentStep - 1;
				}else if($('#step'+currentStep).hasClass('r')) {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep--;
					percent = Math.floor(humanStep/stepCount);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}else {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep--;
					percent = Math.floor(humanStep/stepCount);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}
			}
			
		}else if(formType == 'e'){
			while(i < 5){
				if($('#step'+currentStep).hasClass('p') || $('#step'+currentStep).hasClass('r')){
					currentStep = currentStep - 1;
				}else if($('#step'+currentStep).hasClass('e')) {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep--;
					percent = Math.floor(humanStep/stepCount);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}else {
					$('#step' + currentStep).fadeIn('fast');
					i = i+6;
					humanStep--;
					percent = Math.floor(humanStep/stepCount);
					//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
					return currentStep;
				}
			}
			
		}
		
		$('#step' + currentStep).fadeIn('fast');
		humanStep--;
		percent = Math.floor(humanStep/stepCount);
		//$('.stepCount').html('Step '+humanStep+' of '+stepCount+' - '+percent);
		return currentStep;
	}
	

	
	
	//function for clicking on a box
	function boxClick() {
		var val = $('#step'+currentStep + ' .active .txt').html();
		var key = $('#step'+currentStep).find('input').val();
		var repeat = 0;
			if(postItems.length != 0){
				$.each(postItems, function(index, value){
					if(value.key == key){
						value.val = val;
						//console.log(postItems);
						repeat++;
						nextStep();
					}
				});
				
				if(repeat == 0){
					postItems.push({key: key, val: val});
						//console.log(postItems);
						nextStep();
				}
				
			}else {
				postItems.push({key: key, val: val});
				//console.log(postItems);
				nextStep();
			}
	}
	
	// function to validate box on next button click
	function checkBoxInput(){
		var activecount = 0;
		$('#step' + currentStep).find('.active').each(function() {
			activecount++;
		});
		
		if(activecount == 0){
			alert('Please ' + $('#step'+currentStep).find('.headline').html());
			return false;
		}else {
			
			nextStep();
		}
		return false;
	}
	
	//Handle text input validation and submission on next button click.
	function checkTextInput() {
		var inputerror = 0;
		$('#step'+currentStep).find('input').each(function() {
			if(($(this).val() == '') && (!$(this).hasClass('optional'))){
				inputerror++;
			}
		});
		
		if(inputerror >= 1){
			alert('Please Complete Required Fields');
			return false;
		}else {
			$('#step'+currentStep).find('input').each(function() {
			
				var key = $(this).attr('name');
				var val = $(this).val();
				var repeat = 0;
				$.each(postItems, function(index, value){
					if(value.key == key){
						repeat++;
					}
				});
				if(repeat <= 0){
				postItems.push({key: key, val: val});
				//console.log(postItems);

				}else {
					nextStep();
				}
			
				
			});
			nextStep();
		}
	}
	
	function checkTextDrop() {
		var selecterror = 0;
		$('#step'+currentStep).find('select').each(function() {
			if(($(this).val() == 'Select One') && (!$(this).hasClass('optional'))){
				selecterror++;
			}
		});
		
		if(selecterror >= 1){
			alert('Please Make a Selection');
			return false;
		}else {
			$('#step'+currentStep).find('select').each(function() {
				var key = $(this).attr('name');
				var val = $(this).val();
				var repeat = 0;
				$.each(postItems, function(index, value){
					if(value.key == key){
						value.val = val;
						//console.log(postItems);
						repeat++;
							}
						});
						
						if(repeat == 0){
							postItems.push({key: key, val: val});
							//	console.log(postItems);
						}
			});
		}
		
		var inputerror = 0;
		$('#step'+currentStep).find('input').each(function() {
			if(($(this).val() == '') && (!$(this).hasClass('optional'))){
				inputerror++;
			}
		});
		
		if(inputerror >= 1){
			alert('Please Complete Required Fields');
			return false;
		}else {
			$('#step'+currentStep).find('input').each(function() {
			
				var key = $(this).attr('name');
				var val = $(this).val();
				var repeat = 0;
				$.each(postItems, function(index, value){
					if(value.key == key){
						value.val = val;
						//console.log(postItems);
						repeat++;
							}
						});
						
						if(repeat == 0){
							postItems.push({key: key, val: val});
								//console.log(postItems);
						}
			});
		}
		
		nextStep();
		
		
	}
	
	function checkTextDropLast() {
           
            
		var selecterror = 0;
		$('#step'+currentStep).find('select').each(function() {
			if(($(this).val() == 'Select One') && (!$(this).hasClass('optional'))){
				selecterror++;
			}
		});
		
		if(selecterror >= 1){
			alert('Please Make a Selection');
			return false;
		}else {
			$('#step'+currentStep).find('select').each(function() {
				var key = $(this).attr('name');
				var val = $(this).val();
				var repeat = 0;
				$.each(postItems, function(index, value){
					if(value.key == key){
						value.val = val;
						//console.log(postItems);
						repeat++;
							}
						});
						
						if(repeat == 0){
							postItems.push({key: key, val: val});
								//console.log(postItems);
						}
			});
		}
		
		var inputerror = 0;
		$('#step'+currentStep).find('input').each(function() {
			if(($(this).val() == '') && (!$(this).hasClass('optional'))){
				inputerror++;
			}
		});
		
		if(inputerror >= 1){
			alert('Please Complete Required Fields');
			return false;
		}else {
			$('#step'+currentStep).find('input').each(function() {
			
				var key = $(this).attr('name');
				var val = $(this).val();
				var repeat = 0;
				$.each(postItems, function(index, value){
					if(value.key == key){
						value.val = val;
						//console.log(postItems);
						repeat++;
							}
						});
						
						if(repeat == 0){
							postItems.push({key: key, val: val});
								//console.log(postItems);
						}
			});
			console.log(postItems);
                        var data = {};
                        data.postItems = postItems;
                        
                        var tid = $('#transID').val();
                        var uid = $('#uid').val();
                    $('.continue-but').hide();
                        
			$.ajax({
				type: "POST",
				url: '../../lp4/post.php',
				data: { data: data },
				success: function(res) {
					//console.log(res);
                                                window.location = "thankyou_3.php?tid="+tid+"&uid="+uid;
					/*
					try{
						var r = $.parseJSON(res);
						console.log(r);
					} catch (e){
						console.log("PARSE FAILED");
					}
					//
					console.log(res);
					console.log("Sent Mail");
					*/
					//if (r.status == 0) {
					//	window.location = r.message;
					//}
				}
			});
		}
		
		
		
		
	}
	
	//Handle dropdown validation on next button click
	function checkDropDown() {
		var selecterror = 0;
		$('#step'+currentStep).find('select').each(function() {
			if($(this).val() == 'Select One'){
				selecterror++;
			}
		});
		
		if(selecterror >= 1){
			alert('Please Make a Selection');
			return false;
		}else {
			$('#step'+currentStep).find('select').each(function() {
				var key = $(this).attr('name');
				var val = $(this).val();
				var repeat = 0;
				$.each(postItems, function(index, value){
					if(value.key == key){
						repeat++;
						}
					});
					
					if(repeat <= 0){
						postItems.push({key: key, val: val});
						//console.log(postItems);
					}else {
						nextStep();
					}
			});
			nextStep();
			
				
		}
	}
	
	function getOtherData(){
		var currentDate = new Date();
		var datetime = currentDate.getFullYear() + '-' + (currentDate.getMonth()+1) + '-' + currentDate.getDate() + ' ' + currentDate.getHours() + ':' + currentDate.getMinutes() + ':' + currentDate.getSeconds();
		$('input[name=LeadDate]').val(datetime);
		
		$('#otherData').find('input').each(function() {
			var key = $(this).attr('name');
			var val = $(this).val();
			postItems.push({key: key, val: val});
			//console.log(postItems);
		});
		
			
		
	}
	var num = 1;
	// Hide all steps and appaend forward and next buttons to each step div
   $(this).find('.step').each(function(){
			$(this).attr('id', 'step'+num);
			num++;
					$(this).hide();
					if($(this).hasClass('first')){
						$(this).append('');
					}else if($(this).hasClass('last')){
                                                $(this).append('<a href="#" class="back"><i class="fa fa-arrow-circle-left fa-5"></i></a><div class="submit btn btn-lg btn-primary" style="width: 150px; font-size: 25px;">Submit</div><div style="margin-top: 20px;color: #ADACAC;font-size: 12px;">We take your <a href="../privacy.html" data-toggle="modal" data-toggle="modal" data-target="#myModal">Privacy</a> seriously. By clicking the button, you agree to be matched with <a href="../partners.html" onclick="return popitup("../partners.php")"> partners</a> from our network including New American Funding, loanDepot, and consent (not required as a condition to purchase a good/service) for us and/or them to contact you (including through automated means; e.g. autodialing, text and pre recorded messaging) via telephone, mobile device (including SMS and MMS) and/or email, even if you are on a corporate, state or national Do Not Call Registry.</div>');
					}else {
						$(this).append('<a href="#" class="back"><i class="fa fa-arrow-circle-left fa-5"></i></a> <a href="#" class="fwd"><button class="btn btn-lg btn-primary" width="" height=""><strong style="font-size: 25px;">Next &nbsp;<i class="fa fa-arrow-circle-right"></i></strong></button</a>');
					}
	});
	
	
	 if($('input[name=Type]').val() == 'home-purchase'){
		$('.first').show();
		$('.first .appbox:nth-child(4)').addClass('active');
		postItems.push({key: 'Loan Type', val: 'Home Purchase'});
		stepCount = $('.step:not(.r, .e)').length;
		//console.log(stepCount);
		formType = 'p';
		//console.log(postItems);
		
	}else if($('input[name=Type]').val() == 'home-refinance'){
		$('.first').show();
		$('.first .appbox:nth-child(3)').addClass('active');
		postItems.push({key: 'Loan Type', val: 'Home Refinance'});
		stepCount = $('.step:not(.p, .e)').length;
			//console.log(stepCount);
		formType = 'r';
		//console.log(postItems);
		
	}else if($('input[name=Type]').val() == 'home-equity'){
		$('.first').show();
		$('.first .appbox:nth-child(5)').addClass('active');
		postItems.push({key: 'Loan Type', val: 'Home Equity'});
		stepCount = $('.step:not(.r, .p)').length;
			//console.log(stepCount);
		formType = 'e';
		//console.log(postItems);
		
	}else { 
		$('.first').show();
	}
	//Show first step on the page
	
	//Take care of next button click
	$('.fwd').click(function() {
            //VALIDATE THE FIELDS FIRST!
            
            if($('#state').val()){
                var state = validateState();
                if(!state){
                    window.alert('Please enter a valid State');
                    return false;
                }
            }

            if($('#city').val()){
                var city = validateCity();
                if(!city){
                    window.alert('Please enter a valid city');
                    return false;
                }
            }
            
            if($('#address').val()){
                var addy = validateAddress();
                if(!addy){
                    window.alert('Please enter a valid address');
                    return false;
                }
            }

            //Zip
            var zip = $('#zip').val().toString();
            if($('#address').val()){
                if(zip.length !== 5){
                    window.alert('Please enter a valid zip code');
                    return false;
                }
            }
            
		if($(this).parent().hasClass('box-input')){
			checkBoxInput();
		}else if($(this).parent().hasClass('text-input')){
			checkTextInput();
		}else if($(this).parent().hasClass('textdrop-input')){
			checkTextDrop();
		}else if($(this).parent().hasClass('dropdown-input')){
			checkDropDown();
		}else {
			nextStep();
		}
            
	});
	
	//Take care of back button click
	$('.back').click(function() {
		prevStep();
	});
	
	//Handle Box Clicks
	$('.appbox').click(function() {
		$(this).parent().find('.active').removeClass('active');
		$(this).addClass('active');
		boxClick();
	});
	
	
	$('.submit').click(function() {
            var phone = $('#phone').val().toString().length;
            if($('#phone').val()){
                if(phone === 9 || phone === 10){
                }else{
                    window.alert('Please enter a 9 or 10 digit number');
                    return false;
                }
            }
		var validEmail = validateEmail();
            if(!validEmail){
                window.alert('Please enter a valid email address');
                return false;
            }else{
		getOtherData();
		checkTextDropLast();
            }
		
	});
}